import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { CreateTodoDTO } from './dto/create-todo.dto';
import { Status, Todo } from './interfaces/todo.interface';
import { TodoService } from './todo.service'

@Controller('todos')
export class TodoController {
    constructor(private todoService: TodoService) { }

    @Post()
    async create(@Body() todo: CreateTodoDTO): Promise<Todo> {
        return await this.todoService.create(todo)
    }

    @Get()
    async find(): Promise<Todo[]> {
        return await this.todoService.findAll()
    }

    @Get(':id')
    async findOne(@Param('id') id: string): Promise<Todo> {
        return await this.todoService.findOne(+id)
    }

    @Put(':id')
    async update(
        @Param('id') id: string,
        @Body() toUpdate: CreateTodoDTO,
    ): Promise<Todo> {
        return await this.todoService.update(+id, toUpdate)
    }

    @Delete(':id')
    async delete(@Param('id') id: string): Promise<Status> {
        return await this.todoService.delete(+id)
    }
}
