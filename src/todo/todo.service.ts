import { Injectable, NotFoundException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { Status, Todo } from './interfaces/todo.interface'
import { TodoEntity } from './entities/todo.entity'
import { CreateTodoDTO } from './dto/create-todo.dto'

@Injectable()
export class TodoService {
    constructor(@InjectRepository(TodoEntity)
    private readonly TodoRepository: Repository<TodoEntity>,) { }

    async create(todo: CreateTodoDTO): Promise<Todo> {
        const resp = await this.TodoRepository.save({
            title: todo.title,
            text: todo.text,
            completed: todo.completed
        })
        return resp
    }

    async findAll(): Promise<Todo[]> {
        return await this.TodoRepository.find()
    }

    async findOne(id: number): Promise<Todo> {
        const resp = await this.TodoRepository.findOne(id)
        if (!resp) {
            throw new NotFoundException('Could not find the task with the provided id!')
        }
        return resp
    }

    async update(id: number, toUpdate: CreateTodoDTO): Promise<Todo> {
        const resp = await this.TodoRepository.update(id, toUpdate)
        if (resp.affected === 0) {
            throw new NotFoundException('Could not find the task with the provided id!')
        }
        return await this.TodoRepository.findOne(id)
    }

    async delete(id: number): Promise<Status> {
        const resp = await this.TodoRepository.delete(id)
        if (resp.affected === 0) {
            throw new NotFoundException('Could not find the task with the provided id!')
        }
        return { message: `successfully removed task with id: ${id}` }
    }
}
