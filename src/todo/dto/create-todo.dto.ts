import {
  IsNotEmpty,
  IsString,
  IsInt,
  IsNumber,
  IsOptional,
  IsIn,
  IsBoolean,
} from 'class-validator';

export class CreateTodoDTO {

  @IsNotEmpty()
  @IsString()
  readonly title: string;

  @IsNotEmpty()
  @IsString()
  readonly text: string;

  @IsOptional()
  @IsBoolean()
  readonly completed: boolean;

}